/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1_3;

/**
 *
 * @author natta
 */
public class Lab1_3 {

     public static void main(String[] args) {
        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int m = 3;
        int[] nums2 = {2, 5, 6};
        int n = 3;

        merge(nums1, m, nums2, n);

        for (int num : nums1) {
            System.out.print(num + " ");
        }
        
        System.out.println();
        
        int[] nums11 = {1};
        int o = 1;
        int[] nums22 = {};
        int p = 0;
        merge(nums11, o, nums22, p);
        
        for (int num : nums11) {
            System.out.print(num + " ");
        }
        
        System.out.println();
        
        int[] nums111 = {0};
        int q = 0;
        int[] nums222 = {1};
        int r = 1;
        merge(nums111, q, nums222, r);
        
        for (int num : nums111) {
            System.out.print(num + " ");
        }
        
    }

    private static void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1; 
        int j = n - 1; 
        int k = m + n - 1;

       
        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j]) {
                nums1[k] = nums1[i];
                i--;
            } else {
                nums1[k] = nums2[j];
                j--;
            }
            k--;
        }

        while (j >= 0) {
            nums1[k] = nums2[j];
            j--;
            k--;
        }
    }
}
